#!/bin/sh

# Touch the timestamps on all the files since CVS messes them up
directory=`dirname $0`
touch $directory/configure.in

aclocal  \
&& autoheader \
&& automake -c -a \
&& autoconf2.50 \
&& ./configure $*

