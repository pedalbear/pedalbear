#include <stdio.h>
#include <fcntl.h>
#include <linux/input.h>
#include <glib.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


struct hiddev_event {
  unsigned hid;
  signed int value;
};
static gboolean verbose = FALSE;
gchar * pedals [ ] = { "Left", "Center", "Right" };
gchar * states [ ] = { "Up", "Down" };
gint grace [ ] = { 0, 0, 0 };
#define MAX_GRACE 5

void main_loop ( GKeyFile * config_file, gchar * device );


int main ( int argc, gchar * argv [ ] ) 
{
    static gchar * config_filename = NULL;
    static gchar * device = NULL;
    GKeyFile * config_file;
    static GOptionEntry entries [ ] =
	{
	    { "config", 'f', 0, G_OPTION_ARG_STRING, &config_filename, "Set config file", NULL },
	    { "device", 'd', 0, G_OPTION_ARG_STRING, &device, "Set to alternative device", NULL },
	    { "verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Be verbose", NULL },
	    { NULL }
	};
    GError * error = NULL;
    GOptionContext * context;

    context = g_option_context_new ("- Pedalbear transcription pedal daemon");
    g_option_context_add_main_entries (context, entries, "Pedalbear");
    if ( ! g_option_context_parse ( context, &argc, &argv, &error ) )
    {
	g_print ("option parsing failed: %s\n", error->message);
	exit ( 1 );
    }
    g_option_context_set_help_enabled ( context, TRUE );

    config_file = g_key_file_new ( ) ;
    g_key_file_load_from_file ( config_file,
				( config_filename ? config_filename : "/etc/pedalbear.conf" ),
				G_KEY_FILE_NONE, &error );
    if ( error )
    {
	g_printerr ( "Cannot open config file %s: %s\n", ( config_filename ? 
							   config_filename : "/etc/pedalbear.conf" ),
		     error -> message );
	g_error_free ( error );
	exit ( 1 );
    }			   
    

    main_loop ( config_file, device );
}



/** Main loop.  */
void main_loop ( GKeyFile * config_file, gchar * device )
{
    struct hiddev_event event;
    int fd, i;
    int state [ ] = { 0, 0, 0 }; 
    int new_state [ ] = { 0, 0, 0 }; 
    gchar * filename = NULL;
    fd_set rfds;
    struct timeval tv;

    if ( device )
    {
	filename = g_strdup ( device );
    }
    else
    {
	filename = g_key_file_get_string ( config_file, "Pedalbear", "Device", NULL );
	if ( ! filename )
	{
	    g_printerr ( "No device found in config file and no device specified at command line.\nAborting.\n" );
	    exit ( 1 );
	}
    }

    fd = open ( filename, O_NONBLOCK );
    if ( verbose )
	g_printf ( "Opening device '%s'\n", filename );;
    if ( fd < 0 )
    {
	g_printerr ( "Cannot open device %s: %s.\n", filename, g_strerror ( errno ) );
	exit ( 1 );
    }
    g_free ( filename );

    while ( 1 ) 
    {
	int retval;

	FD_ZERO ( &rfds );
	FD_SET ( fd, &rfds );
		    
	tv.tv_sec = 0;
	tv.tv_usec = 50000;
		    
	retval = select ( fd + 1, &rfds, NULL, NULL, &tv );

	bcopy ( new_state, state, sizeof ( int ) * 3 );

	if ( retval )
	{
	    for ( i = 0 ; i < 3 ; i ++ ) 
	    {
		int rcount = read ( fd, &event, sizeof ( struct hiddev_event ) );
		    
		if ( rcount > 0 && ( event.hid & 0xFF ) == ( i + 1 ) ) 
		{
		    new_state [ i ] = event.value;
		} 
		else if ( rcount <= 0 ) 
		{
		    usleep ( 5 * 1000 );
		}
	    }
	    if ( verbose )
		printf ( "New states : %d,%d,%d\n", new_state [ 0 ], new_state [ 1 ], new_state [ 2 ] );
	}

	handle_states ( config_file, state, new_state );
    }
}


handle_states ( GKeyFile * config_file, int state [ ], int new_state [ ] )
{
    gint i;

    for ( i = 0 ; i < 3 ; i ++ ) 
    {
	gchar * group_name = g_strconcat ( pedals [ i ], states [ new_state [ i ] ], NULL );

	if ( state [ i ] == new_state [ i ] == 1 && g_key_file_get_boolean ( config_file, group_name, "Repeat", 0 ) )
	{
	    if ( grace [ i ] < MAX_GRACE )
	    {
		grace [ i ] ++;
		continue;
	    }
	}

	if ( ! new_state [ i ] )
	{
	    grace [ i ] = 0;
	}

	if ( new_state [ i ] != state [ i ] || 
	     ( new_state [ i ] == 1 && g_key_file_get_boolean ( config_file, group_name, "Repeat", 0 ) ) )
	{
	    if ( verbose )
		g_printf ( "%s pedal : %s vs %s (grace %d)\n", group_name, states [ new_state [ i ] ], states [ state [ i ] ], 
			   grace [ i ] );

	    if ( g_key_file_has_group ( config_file, group_name ) )
	    {
		gchar * cmd, * key, * msg, * file;
		gboolean create;
		cmd = g_key_file_get_string ( config_file, group_name, "Exec", NULL );
		key = g_key_file_get_string ( config_file, group_name, "Key", NULL );
		file = g_key_file_get_string ( config_file, group_name, "File", NULL );
		msg = g_key_file_get_string ( config_file, group_name, "Message", NULL );
		create = g_key_file_get_boolean ( config_file, group_name, "Create", 0 );

		/* Message appended to file */
		if ( file && msg )
		{
		    int out = open ( file, O_WRONLY | O_APPEND | ( create ? O_CREAT : 0 ), 0644 );
		    if ( verbose )
			g_printf ( "Appending data to %s : '%s'\n", file, msg );
		    if ( out >= 0 )
		    {
			if ( write ( out, msg, strlen ( msg ) ) < 0 )
			{
			    g_printerr ( "Cannot write to file %s : %s\n", file, g_strerror ( errno ) );
			}
			close ( file );
		    }
		    else
			g_printerr ( "Cannot write to file %s : %s\n", file, g_strerror ( errno ) );

		}

		/* X keys sent */
		if ( key  )
		{
		    if ( verbose )
			g_printf ( "Sending key '%s'\n", key );
		    sendkey ( key );
		}
		    
		/* Command executed */
		if ( cmd )
		{
		    if ( verbose )
			g_printf ( "Executing %s\n", cmd );
		    g_spawn_command_line_sync ( cmd, NULL, NULL, NULL, NULL );
		}
	    }
	}

	g_free ( group_name );
    }
}
